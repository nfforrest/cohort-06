import sys

total = 0.0

for arg in sys.argv[1:]:
    try:
        arg = float(arg)
    except ValueError:
        print("There was an issue")
    total = total + arg

print(total)
