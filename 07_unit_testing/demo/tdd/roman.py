
def roman_to_decimal(roman_numeral):
    ROMAN_DIGITS = {
        'V': 5,
        'X': 10,
        'L': 50,
        'I': 1,
        'C': 100,
        'D': 500,
        'M': 1000,
    }
    total = 0
    prev = None
    if '' == roman_numeral:
        raise ValueError("Empty String")
    for digit in roman_numeral:
        try:
            current = ROMAN_DIGITS[digit]
        except KeyError:
            raise ValueError("Invalid Roman Numeral")
        total += current
        if prev and prev < current:
            total -= 2 * prev
        prev = current
    return total
