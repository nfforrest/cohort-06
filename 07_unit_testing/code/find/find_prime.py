#!/usr/bin/env python3

def find_prime(prime_num):
    for num in range(2, prime_num+1):
        if prime_num != num and prime_num % num == 0:
            return False
        return True
