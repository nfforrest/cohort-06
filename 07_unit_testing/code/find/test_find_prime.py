import pytest
from find_prime import find_prime

def test_find_prime_one():
    assert find_prime(1) == False
    
def test_find_prime_two():
    assert find_prime(2) == True
    
def test_find_prime_four():
    assert find_prime(4) == False  
    
def test_find_prime_nine():
    assert find_prime(9) == False
    
def test_find_prime_eleven():
    assert find_prime(11) == True
