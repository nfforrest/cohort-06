#!/usr/bin/env python3
import requests
import json
import datetime
import pytz
from pprint import pprint
from requests.auth import HTTPBasicAuth
ASTRONOMYAPI_ID = "c24f965e-d754-4e7f-ad38-06c793c2d279"
ASTRONOMYAPI_SECRET = "b586c68a0970564b265d639a9c730e1f1e8c7ac5e098e76d79c3e9f6807ce68804bf55e9f765943365a28b2766decc9b25cb7783f8ff3e984108e74528e04b91a6a88c8ec91055661d17d04c40de5213a98f773d3b8c586d5a78b10b9a517861d60985dcbcb20f767b614fab98d15301"
def get_observer_location():
    """Returns the longitude and latitude for the location of this machine.
    Returns:
    str: latitude
    str: longitude"""
    ob_location = requests.get("http://ip-api.com/json/?fields=lat,lon,query")
    json = ob_location.json()
    latitude = json.get("lat")
    longitude = json.get("lon")
    print(ob_location.text, latitude, longitude)
    return latitude, longitude
def get_sun_position(latitude, longitude):
    """Returns the current position of the sun in the sky at the specified location
    Parameters:
    latitude (str)
    longitude (str)
    Returns:
    float: azimuth
    float: altitude"""
    current_time = datetime.datetime.now(pytz.timezone('US/Eastern'))
    fmt = '%H:%M:%S'
    time = current_time.strftime(fmt)
    query = {
    "latitude": latitude,
    "longitude": longitude,
    "elevation": 0,
    "from_date": "2021-09-30",
    "to_date": "2021-10-01",
    "time": time
    }
    sun_location = requests.get('https://api.astronomyapi.com/api/v2/bodies/positions/sun', auth=HTTPBasicAuth(ASTRONOMYAPI_ID, ASTRONOMYAPI_SECRET), params = query)
#   json = sun_location.json()
    sun_data = sun_location.json()["data"]["table"]["rows"][0]["cells"][0]["position"]["horizonal"]
    #   sun_data = sun_location.json()["data"]["table"]["rows"]["postion"]
    azimuth = sun_data["azimuth"]["degrees"]
    altitude = sun_data["altitude"]["degrees"]
    print(azimuth, altitude)
    return azimuth, altitude
def print_position(azimuth, altitude):
    """Prints the position of the sun in the sky using the supplied coordinates
    Parameters:
    azimuth (float)
    altitude (float)"""
    print("The Sun is currently at: ", azimuth, altitude)
if __name__ == "__main__":
    latitude, longitude = get_observer_location()
    azimuth, altitude = get_sun_position(latitude, longitude)
    print_position(azimuth, altitude)